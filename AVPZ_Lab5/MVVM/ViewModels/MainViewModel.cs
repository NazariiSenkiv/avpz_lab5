﻿using AVPZ_Lab5.Infrastructure.Commands;
using AVPZ_Lab5.MVVM.Models;
using AVPZ_Lab5.MVVM.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;

namespace AVPZ_Lab5.MVVM.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public ObservableCollection<InputModel> TechnicalRiskEvents { get; set; }
        public ObservableCollection<InputModel> ValueRiskEvents { get; set; }
        public ObservableCollection<InputModel> PlanRiskEvents { get; set; }
        public ObservableCollection<InputModel> ProcessRealisationRiskEvents { get; set; }

        public ObservableCollection<ExpertMarksModel> ExpertsProbabilityMarks { get; set; }
        public ObservableCollection<ExpertMarksModel> ExpertsLossMarks { get; set; }

        public ObservableCollection<RealisationPriceRow> RealisationPriceRows { get; set; }

        public ObservableCollection<RiskTableRow> RisksTable { get; set; }
        public ObservableCollection<RiskCostTableRow> RiskCostsTable { get; set; }

        public ObservableCollection<Tab4TableModel> Tab4TableModels { get; set; }

        public LambdaCommand UpdateTab2Command { get; set; }
        public LambdaCommand UpdateTab3Command { get; set; }
        public LambdaCommand UpdateTab4Command { get; set; }


        private double _min;
        public double Min
        {
            get => _min; 
            set => Set(ref _min, value);
        }

        private double _max;
        public double Max
        {
            get => _max;
            set => Set(ref _max, value);
        }

        private double _mpr;
        public double MPR
        {
            get => _mpr;
            set => Set(ref _mpr, value);
        }

        public MainViewModel()
        {
            TechnicalRiskEvents = new ObservableCollection<InputModel>();
            #region TechnicalRiskEvents Fill
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "затримки у постачанні обладнання, необхідного для підтримки процесу розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "затримки у постачанні інструментальних засобів, необхідних для підтримки процесу розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "небажання команди виконавців використовувати інструментальні засоби для підтримки процесу розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "формування запитів на більш потужні інструментальні засоби розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "відмова команди виконавців від CASE-засобів розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "неефективність програмного коду, згенерованого CASE-засобами розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "неможливість інтеграції CASE-засобів з іншими інструментальними засобами для підтримки процесу розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "недостатня продуктивність баз(и) даних для підтримки процесу розроблення ПЗ;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "програмні компоненти, які використовують повторно в ПЗ, мають дефекти та обмежені функціональні можливості;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "швидкість виявлення дефектів у програмному коді є нижчою від раніше запланованих термінів;"
                }
            );
            TechnicalRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "поява дефектних системних компонент, які використовують для розроблення ПЗ;"
                }
            );
            #endregion

            ValueRiskEvents = new ObservableCollection<InputModel>();
            #region ValueRiskEvents Fill
            ValueRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "недо(пере)оцінювання витрат на реалізацію програмного проекту (надмірно низька вартість);"
                }
            );
            ValueRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "фінансові ускладнення у компанії-замовника ПЗ;"
                }
            );
            ValueRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "фінансові ускладнення у компанії-розробника ПЗ;"
                }
            );
            ValueRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "змен(збіль)шення бюджету програмного проекта з ініціативи компанії-замовника ПЗ під час його реалізації;"
                }
            );
            ValueRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "висока вартість виконання повторних робіт, необхідних для зміни вимог до ПЗ;"
                }
            );
            ValueRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "реорганізація структурних підрозділів у компанії-замовника ПЗ;"
                }
            );
            ValueRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "реорганізація команди виконавців у компанії-розробника ПЗ;"
                }
            );

            #endregion

            PlanRiskEvents = new ObservableCollection<InputModel>();
            #region PlanRiskEvents Fill
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "зміни графіка виконання робіт з боку замовника чи розробника ПЗ;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "порушення графіка виконання робіт з боку компанії-розробника ПЗ;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "потреба зміни користувацьких вимог до ПЗ з боку компанії-замовника ПЗ;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "потреба зміни функціональних вимог до ПЗ з боку компанії-розробника ПЗ;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "потреба виконання великої кількості повторних робіт, необхідних для зміни вимог до ПЗ;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "недо(пере)оцінювання тривалості етапів реалізації програмного проекту з боку компанії-замовника ПЗ;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "остаточний розмір ПЗ значно перевищує (менший від) заплановані(их) його характеристики;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "поява на ринку аналогічного ПЗ до виходу замовленого;"
                }
            );
            PlanRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "поява на ринку більш конкурентоздатного ПЗ;"
                }
            );

            #endregion

            ProcessRealisationRiskEvents = new ObservableCollection<InputModel>();
            #region ProcessRealisationRiskEvents Fill
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "низький моральний стан персоналу команди виконавців ПЗ;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = " низька взаємодія між членами команди виконавців ПЗ;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = " пасивність керівника (менеджера) програмного проекту;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = " недостатня компетентність керівника (менеджера) програмного проекту;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "незадоволеність замовника результатами етапів реалізації програмного проекту;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "недостатня кількість фахівців у команді виконавців ПЗ з необхідним професійним рівнем;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "хвороба провідного виконавця в найкритичніший момент розроблення ПЗ;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "одночасна хвороба декількох виконавців підчас розроблення ПЗ;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "неможливість організації необхідного навчання персоналу команди виконавців ПЗ;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "зміна пріоритетів у процесі управління програмним проектом;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "недо(пере)оцінювання необхідної кількості розробників (підрядників і субпідрядників) на етапах життєвого циклу розроблення ПЗ;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "недостатнє (надмірне) документування результатів на етапах реалізації програмного проекту;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "нереалістичне прогнозування результатів на етапах реалізації програмного проекту;"
                }
            );
            ProcessRealisationRiskEvents.Add(
                new InputModel()
                {
                    TextDescription = "недостатній професійний рівень представників від компанії-замовника ПЗ."
                }
            );

            #endregion

            ExpertsProbabilityMarks = new ObservableCollection<ExpertMarksModel>();
            #region ExpertsProbabilityMarks Fill
            ExpertsProbabilityMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Технічні", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            ExpertsProbabilityMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Вартісні", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            ExpertsProbabilityMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Планові", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            ExpertsProbabilityMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Реалізації процесів", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            #endregion

            ExpertsLossMarks = new ObservableCollection<ExpertMarksModel>();
            #region ExpertsLossMarks Fill
            ExpertsLossMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Технічні", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            ExpertsLossMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Вартісні", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            ExpertsLossMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Планові", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            ExpertsLossMarks.Add(new ExpertMarksModel(10) { ExpertArea = "Реалізації процесів", ExpertsMark = GenerateRandomIntArray(10, 5, 11)});
            #endregion

            RealisationPriceRows = new ObservableCollection<RealisationPriceRow>();
            #region RealisationPriceRows Fill
            var genArr = GenerateRandomArray(4, 200, 500);
            RealisationPriceRows.Add(new RealisationPriceRow() 
            { 
                Prices = genArr, 
            });

            RealisationPriceRows.Add(new RealisationPriceRow()
            {
                Prices = new double[] { 0, 0, 0, 0 },
            });
            #endregion

            RisksTable = new ObservableCollection<RiskTableRow>();
            FillRisksTable();

            RiskCostsTable = new ObservableCollection<RiskCostTableRow>();
            FillRiskCostsTable();

            Tab4TableModels = new ObservableCollection<Tab4TableModel>();
            FillTab4Table();

            UpdateTab2Command = new LambdaCommand(o =>
            {
                RisksTable.Clear();
                FillRisksTable();
            });

            UpdateTab3Command = new LambdaCommand(o =>
            {
                RiskCostsTable.Clear();
                FillRiskCostsTable();
            });

            UpdateTab4Command = new LambdaCommand(o =>
            {
                Tab4TableModels.Clear();
                FillTab4Table();
            });
        }

        private void FillTab4Table()
        {
            int i = 0;
            for (int subTableId = 0; subTableId < 4; subTableId++)
            {
                var header = new Tab4TableModel()
                {
                    TextDescription = GetTableNameById(subTableId),
                    Value = 0,
                    ExtraPrice = RiskCostsTable[i].ExtraPrice,
                    IsBold = true,
                };
                Tab4TableModels.Add(header);
                i++;

                int rowsCount = GetRowsCountByTableId(subTableId);
                for (int row = 0; row < rowsCount; row++)
                {
                    double val = GetRiskEventsById(subTableId)[row].Value;

                    Tab4TableModels.Add(new Tab4TableModel()
                    {
                        TextDescription = GetRiskEventsById(subTableId)[row].TextDescription,
                        Value = val,
                        ExtraPrice = RiskCostsTable[i].ExtraPrice,
                    });
                    i++;
                }
            }

            Tab4TableModel.Max = Tab4TableModels.Where(m => !m.IsBold).Select(m=>m.ExtraPrice).Max();
            Max = Tab4TableModel.Max;

            Tab4TableModel.Min = Tab4TableModels.Where(m => !m.IsBold).Select(m=>m.ExtraPrice).Min();
            Min = Tab4TableModel.Min;

            Tab4TableModel.MPR = (Tab4TableModel.Max - Tab4TableModel.Min) / 3;
            MPR = Tab4TableModel.MPR;
        }

        private string GetTableNameById(int id)
        {
            switch (id)
            {
                case 0:
                    return "Множина настання технічних ризикових подій";
                case 1:
                    return "Множина настання вартісних ризикових подій";
                case 2:
                    return "Множина настання планових ризикових подій";
                case 3:
                    return "Множина настання ризикових подій реалізації процесів і процедур управління програмним проектом";
                default:
                    return "unknown";
            }
        }

        private ObservableCollection<InputModel> GetRiskEventsById(int id)
        {
            switch (id)
            {
                case 0:
                    return TechnicalRiskEvents;
                case 1:
                    return ValueRiskEvents;
                case 2:
                    return PlanRiskEvents;
                case 3:
                    return ProcessRealisationRiskEvents;
                default:
                    return null;
            }
        }
        private int GetRowsCountByTableId(int id)
        {
            switch (id)
            {
                case 0:
                    return 11;
                case 1:
                    return 7;
                case 2:
                    return 9;
                case 3:
                    return 14;
                default:
                    return 0;
            }
        }

        private void FillRisksTable()
        {
            for (int subTableId = 0; subTableId < 4; subTableId++)
            {
                var header = new RiskTableRow()
                {
                    Text = GetTableNameById(subTableId),
                    Value = 0,
                    Coefficients = GetTableHeaderCoefficients(subTableId),
                    Probability = 0,
                    IsBold = true,
                };
                RisksTable.Add(header);

                int rowsCount = GetRowsCountByTableId(subTableId);
                for (int row = 0; row < rowsCount; row++)
                {
                    double val = GetRiskEventsById(subTableId)[row].Value;
                    double[] coefs = GenerateCoefficients(GetRiskEventsById(subTableId)[row].Value, header);

                    RisksTable.Add(new RiskTableRow()
                    {
                        Text = GetRiskEventsById(subTableId)[row].TextDescription,
                        Value = val,
                        Coefficients = coefs,
                        Probability = Math.Round(coefs.Skip(10).Sum() / header.Coefficients.Take(10).Sum(), 2)
                    });
                }

                CalculateHeaderCoefficients(header, rowsCount);
            }
        }

        private void FillRiskCostsTable()
        {
            for (int subTableId = 0; subTableId < 4; subTableId++)
            {
                var header = new RiskCostTableRow()
                {
                    Text = GetTableNameById(subTableId),
                    Value = 0,
                    Coefficients = GetCostTableHeaderCoefficients(subTableId),
                    IsBold = true,
                };
                RiskCostsTable.Add(header);

                int rowsCount = GetRowsCountByTableId(subTableId);
                for (int row = 0; row < rowsCount; row++)
                {
                    double val = GetRiskEventsById(subTableId)[row].Value;
                    double[] coefs = GenerateCostTableCoefficients(
                                        GetRiskEventsById(subTableId)[row].Value,
                                        subTableId,
                                        header);

                    RiskCostsTable.Add(new RiskCostTableRow()
                    {
                        Text = GetRiskEventsById(subTableId)[row].TextDescription,
                        Value = val,
                        Coefficients = coefs
                    });
                }

                CalculateCostHeaderCoefficients(header, subTableId, rowsCount);
                CalculateCosts(rowsCount, subTableId);
            }
        }

        private void CalculateCosts(int rowsCount, int subTableId)
        {
            var numbers = GenerateRandomArray(rowsCount, 10.0, 30.0);
            double sum = numbers.Sum();

            numbers = numbers.Select(n => n / sum).ToArray();

            int startRowId = RiskCostsTable.Count - rowsCount;

            var tableHeader = RiskCostsTable[startRowId - 1];

            double extraPriceSum = 0;
            double finalPriceSum = 0;
            for (int i = 0; i < rowsCount; i++)
            {
                var currentRow = RiskCostsTable[startRowId + i];

                currentRow.Cost = Math.Round(numbers[i] * tableHeader.Cost, 2);
                
                currentRow.ExtraPrice =
                    Math.Round(currentRow.Cost * currentRow.Coefficients.Skip(10).Sum() 
                                / tableHeader.Coefficients.Take(10).Sum(), 2);
                extraPriceSum += currentRow.ExtraPrice;

                currentRow.FinalPrice = Math.Round(currentRow.ExtraPrice + currentRow.Cost, 2);
                finalPriceSum += currentRow.FinalPrice;
            }

            tableHeader.ExtraPrice = extraPriceSum;
            tableHeader.FinalPrice = Math.Round(finalPriceSum, 2);

            var prevPrices = RealisationPriceRows[1].Prices;
            RealisationPriceRows.RemoveAt(1);
            RealisationPriceRows.Add(new RealisationPriceRow() { Prices = prevPrices});
            RealisationPriceRows[1].Prices[subTableId] = tableHeader.FinalPrice;
        }

        private void CalculateHeaderCoefficients(RiskTableRow header, int rowsCount)
        {
            int firstSubtableRowId = RisksTable.Count - rowsCount;

            for (int col = 10; col < 20; col++)
            {
                double sum = 0;
                for (int row = 0; row < rowsCount; row++)
                {
                    sum += RisksTable[firstSubtableRowId + row].Coefficients[col];
                }

                double average = sum / rowsCount;

                header.Coefficients[col] = Math.Round(average / header.Coefficients[col-10], 2);
            }

            // calculate average probability
            double probabilitySum = 0;
            for (int row = 0; row < rowsCount; row++)
            {
                probabilitySum += RisksTable[firstSubtableRowId + row].Probability;
            }

            header.Probability = Math.Round(probabilitySum / rowsCount, 2);
        }

        private void CalculateCostHeaderCoefficients(RiskCostTableRow header, int subTableId, int rowsCount)
        {
            header.Cost = RealisationPriceRows[0].Prices[subTableId];

            int subtableFirstRowId = RiskCostsTable.Count - rowsCount;

            for (int col = 10; col < 20; col++)
            {
                double sum = 0;
                for (int row = 0; row < rowsCount; row++)
                {
                    sum += RiskCostsTable[subtableFirstRowId + row].Coefficients[col];
                }

                double average = sum / rowsCount;

                header.Coefficients[col] = Math.Round(average / header.Coefficients[col - 10], 2);
            }
        }

        private double[] GetTableHeaderCoefficients(int tableId)
        {
            double[] coefficients = new double[20];

            for (int i = 0; i < 10; i++)
            {
                coefficients[i] = ExpertsProbabilityMarks[tableId].ExpertsMark[i];
            }

            return coefficients;
        }

        private double[] GetCostTableHeaderCoefficients(int tableId)
        {
            double[] coefficients = new double[20];

            for (int i = 0; i < 10; i++)
            {
                coefficients[i] = ExpertsLossMarks[tableId].ExpertsMark[i];
            }

            return coefficients;
        }

        private double[] GenerateCoefficients(double value, RiskTableRow header)
        {
            double[] array = GenerateRandomArray(20);

            for (int i = 10; i < 20; i++)
            {
                array[i] = value > 0 ? Math.Round(array[i - 10] * header.Coefficients[i - 10], 2) : 0;
            }

            return array;
        }

        private double[] GenerateCostTableCoefficients(double value, int subtableId, RiskCostTableRow header)
        {
            double[] array = GenerateRandomArray(20);

            for (int i = 10; i < 20; i++)
            {
                // TODO: ExpertsMark maybe change to ExpertLoss
                array[i] = value > 0 
                    ? Math.Round(array[i - 10] * ExpertsProbabilityMarks[subtableId].ExpertsMark[i - 10], 2) 
                    : 0;
            }

            return array;
        }

        private double[] GenerateRandomArray(int count)
        {
            Random random = new Random();
            double[] values = new double[count];

            for (int i = 0; i < count; ++i)
            {
                values[i] = Math.Round(random.NextDouble()*0.7 + random.NextDouble() * 0.1, 2);
            }

            return values;
        }

        private double[] GenerateRandomArray(int count, double lowerBound, double upperBound)
        {
            Random random = new Random();
            double[] values = new double[count];

            for (int i = 0; i < count; ++i)
            {
                values[i] = Math.Round(random.NextDouble() * (upperBound-lowerBound) + lowerBound, 2);
            }

            return values;
        }

        private int[] GenerateRandomIntArray(int count, int lowerBound, int upperBound)
        {
            Random random = new Random();
            int[] values = new int[count];

            for (int i = 0; i < count; ++i)
            {
                values[i] = random.Next(lowerBound, upperBound);
            }

            return values;
        }
    }
}
